# My Project Management

#### Description

MTLH-Soure

#### Software Architecture

Software architecture description

#### Installation

1.  branch[develop]:Pull remote code to local and run "npm install"
2.  branch[feature]:Pull remote code to local and run "npm install"
3.  branch[electron]:Pull remote code to local and run "npm install"
4.  When the download is complete, run "npm run start"

#### Instructions

1.  branch[develop]:A basic webpage audio-visual interaction project
2.  branch[feature]:Web Audiovisual Interaction Project Development Upgrade
3.  branch[electron]:A desktop application

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request

#### Gitee Feature

1.  You can use Readme_XXX.md to support different languages, such as Readme_en.md, Readme_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
